import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {BaseApi} from '../../../shared/core/base-api';
import {Bill} from '../models/bill.model';

@Injectable()
export class BillService extends BaseApi {
  constructor(public http: HttpClient) {
    super(http);
  }

  getBill(): Observable<Bill> {
    return this.get('bill');
  }

  updateBill(bill: Bill): Observable<Bill> {
    return this.put('bill', bill);
  }

  getCurrency(base: string = 'RUB'): Observable<any> {
    return this.http
      .get<any>(`http://data.fixer.io/api/latest?access_key=ba1cd601b3655e95f9bd5af8ff2cd566&format=1`)
      .pipe(
        map((response: any) => this.convertTo(response, base))
      );
  }

  private convertTo(response: any, convertTo: string): any {
    if (response && response.rates && response.rates[convertTo]) {
      const converted: any = {
        ...response,
        base: convertTo,
        rates: {}
      };
      Object.keys(response.rates).forEach((key: string) => {
        converted.rates[key] = response.rates[key] / response.rates[convertTo];
      });
      return converted;
    } else {
      console.error('Отсутствует валюта конвертации');
      return response;
    }
  }
}
