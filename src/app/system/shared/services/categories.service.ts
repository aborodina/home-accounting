import {BaseApi} from '../../../shared/core/base-api';
import {Category} from '../models/category.model';
import {Observable} from 'rxjs';

export class CategoriesService extends BaseApi {

  addCategory(category: Category): Observable<Category> {
    return this.post('categories', category);
  }

  getCategories(): Observable<Category[]> {
    return this.get('categories');
  }

  updateCategory(category: Category): Observable<Category> {
    return this.put(`categories/${category.id}`, category);
  }

  getCategoriesById(id: number): Observable<Category> {
    return this.get(`categories/${id}`);
  }
}
