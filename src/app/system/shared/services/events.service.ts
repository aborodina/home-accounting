import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {APPEvent} from '../models/event.model';
import {BaseApi} from '../../../shared/core/base-api';

@Injectable()
export class EventsService extends BaseApi {

  addEvent(event: APPEvent): Observable<APPEvent> {
    return this.post('events', event);
  }

  getEvents(): Observable<APPEvent[]> {
    return this.get('events');
  }

  getEventById(id: string): Observable<APPEvent> {
    return this.get(`events/${id}`);
  }
}

