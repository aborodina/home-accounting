import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {mergeMap} from 'rxjs/operators';
import {Observable, Subscription} from 'rxjs';

import {APPEvent} from '../../shared/models/event.model';
import {CategoriesService} from '../../shared/services/categories.service';
import {Category} from '../../shared/models/category.model';
import {EventsService} from '../../shared/services/events.service';

@Component({
  selector: 'app-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.css']
})
export class HistoryDetailComponent implements OnInit, OnDestroy {
  event: APPEvent;
  category: Category;

  isLoaded = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private eventsService: EventsService,
              private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.sub = this.route.params.pipe(
      mergeMap<{ id: string }, Observable<APPEvent>>(
        (params: { id: string }) => this.eventsService.getEventById(params.id)),
      mergeMap<APPEvent, Observable<Category>>((event: APPEvent) => {
        this.event = event;
        return this.categoriesService.getCategoriesById(event.category);
      }))
      .subscribe((category: Category) => {
        this.category = category;
        this.isLoaded = true;
      });
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }
}
