import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {BaseApi} from '../core/base-api';
import {User} from '../models/user.model';

@Injectable()
export class UsersService extends BaseApi {
  getUserByEmail(email: string): Observable<User> {
    return this.get(`users?email=${email}`).pipe(
      map((users: User[]) => users[0] ? users[0] : undefined)
    );
  }

  createNewUser(user: User): Observable<User> {
    return this.post('users', user);
  }
}
