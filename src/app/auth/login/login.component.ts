import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import {AuthService} from '../../shared/services/auth.service';
import {Message} from '../../shared/models/message.model';
import {User} from '../../shared/models/user.model';
import {UsersService} from '../../shared/services/users.service';
import {fadeStateTrigger} from '../../shared/animations/fade.animation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeStateTrigger]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  message: Message;

  constructor(private usersService: UsersService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.message = new Message('danger', '');
    this.route.queryParams
      .subscribe((params: {
        accessDenied: any;
        nowCanLogin: boolean
      }) => {
        if (params.nowCanLogin) {
          this.showMessage({
            text: 'Теперь вы можете войти в систему',
            type: 'success'
          });
        } else if (params.accessDenied) {
          this.showMessage({
              text: 'Для работы с системой вам нужно войти',
              type: 'warning'
            }
          );
        }
      });

    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }

  private showMessage(message: Message) {
    this.message = message;
    window.setTimeout(() => {
      this.message.text = '';
    }, 5000);
  }

  onSubmit() {
    const formData = this.form.value;
    this.usersService.getUserByEmail(formData.email)
      .subscribe((user: User) => {
        if (user) {
          if (user.password === formData.password) {
            this.message.text = '';
            window.localStorage.setItem('user', JSON.stringify(user));
            this.authService.login();
            this.router.navigate(['/system', 'bill']);
          } else {
            this.showMessage({
              text: 'Введите правильный пароль',
              type: 'danger'
            });
          }
        } else {
          this.showMessage({
            text: 'Такого пользовотеля не существует',
            type: 'danger'
          });
        }
      });
  }
}
